package specialization.secondhomework.task1;
//1. Реализовать метод, который на вход принимает ArrayList<T>,
// а возвращает набор уникальных элементов этого массива.
// Решить используя коллекции.

import java.util.ArrayList;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        ArrayList<Integer> a = new ArrayList<>();
        a.add(32);
        a.add(47);
        a.add(1);
        a.add(32);
        a.add(1);

        ArrayList<String> b = new ArrayList<>();
        b.add("Иванов");
        b.add("Петров");
        b.add("Сидоров");
        b.add("Петров");

        TreeSet<Integer> aSet = createCollection(a);
        TreeSet<String> bSet = createCollection(b);

        System.out.println(aSet);
        System.out.println(bSet);

    }

    public static <T> TreeSet<T> createCollection(ArrayList<T> something) {
        return new TreeSet<>(something);
    }
}
