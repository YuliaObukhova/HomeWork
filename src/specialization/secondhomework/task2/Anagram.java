package specialization.secondhomework.task2;

//2. С консоли на вход подается две строки s и t.
// Необходимо вывести true, если одна строка является
// валидной анаграммой другой строки и false иначе.
// Анаграмма — это слово или фраза, образованная
// путем перестановки букв другого слова или фразы,
// обычно с использованием всех исходных букв ровно один раз.


import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите первое слово:");
        String s = sc.nextLine();
        System.out.println("Введите второе слово:");
        String t = sc.nextLine();
        System.out.println(isAnagram(s, t));
    }

    /**
     * @param firstWord первое слово
     * @param secondWord второе слово
     * @return является ли второе слово амограммой первого
     */
    public static boolean isAnagram(String firstWord, String secondWord) {
        if (firstWord.length() != secondWord.length()) {
            return false;
        } else {
            char[] firstWordArray = firstWord.toLowerCase().toCharArray();
            char[] secondWordArray = secondWord.toLowerCase().toCharArray();
            return Arrays.equals(firstWordArray, secondWordArray);
        }
    }

}
