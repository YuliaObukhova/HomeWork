package specialization.secondhomework.task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document {
    private final int id;
    private final String name;
    private final int pageCount;

    Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> hashMap = new HashMap<>(documents.size());
        for (Document doc : documents) {
            hashMap.put(doc.getId(), doc);
        }
        return hashMap;

    }
}