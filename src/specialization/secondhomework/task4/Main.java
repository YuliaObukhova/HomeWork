package specialization.secondhomework.task4;

//В некоторой организации хранятся документы (см. класс Document).
// Сейчас все документы лежат в ArrayList,
// из-за чего поиск по id документа выполняется неэффективно.
// Для оптимизации поиска по id,
// необходимо помочь сотрудникам перевести хранение документов из ArrayList в HashMap.
//Реализовать метод со следующей сигнатурой:
//public Map<Integer, Document> organizeDocuments(List<Document> documents)

import java.util.ArrayList;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        ArrayList<Document> documents = new ArrayList<>();
        documents.add(new Document(1, "Order", 123));
        documents.add(new Document(3, "Minutes", 67));
        documents.add(new Document(2, "Agreement", 2));
        documents.add(new Document(5, "Internal regulations", 200));
        documents.add(new Document(4, "Campaign", 15));

        Map<Integer, Document> oraganizedDocuments = Document.organizeDocuments(documents);

        System.out.println("Документ id 2 - " + oraganizedDocuments.get(2).getName());
        System.out.println("Документ id 5 - " + oraganizedDocuments.get(5).getName());
        System.out.println("Документ id 3 - " + oraganizedDocuments.get(3).getName());

    }
}
