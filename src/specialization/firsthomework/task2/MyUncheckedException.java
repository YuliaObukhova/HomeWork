package specialization.firsthomework.task2;
//Создать собственное исключение MyUncheckedException,
// являющееся непроверяемым.

public class MyUncheckedException extends ArrayIndexOutOfBoundsException {
    public MyUncheckedException(String message) {
        super(message);
    }
}
