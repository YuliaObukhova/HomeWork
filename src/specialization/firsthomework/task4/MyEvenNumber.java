package specialization.firsthomework.task4;

public class MyEvenNumber {

    private int n;

    public MyEvenNumber(int n) {
        if (n % 2 == 0){
            this.n = n;
        }
       else {
           throw new IllegalArgumentException();
        }
    }

    public int getN() {
        return n;
    }
}
