package specialization.firsthomework.task4;

import java.util.Scanner;

//Создать класс MyEvenNumber, который хранит четное число int n.
// Используя исключения, запретить создание инстанса
// MyPrimeNumber с нечетным числом.
public class Main {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        try {
            MyEvenNumber myPrimeNumber = new MyEvenNumber(n);
            System.out.println(myPrimeNumber.getN());

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Невозможно создать инстанс с нечётным числом");
        }
    }
}
