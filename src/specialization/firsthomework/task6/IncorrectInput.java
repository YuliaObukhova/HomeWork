package specialization.firsthomework.task6;

public class IncorrectInput extends Exception {
    IncorrectInput(String message) {
        super(message);
    }
}

