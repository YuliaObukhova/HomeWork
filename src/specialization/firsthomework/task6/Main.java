package specialization.firsthomework.task6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IncorrectInput {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите имя: ");
        String strName = sc.nextLine();
        try {
            FormValidator.checkName(strName);
        } catch (IncorrectInput incorrectInput) {
            System.out.println(incorrectInput.getMessage());
        }
        System.out.println();

        System.out.println("Введите дату рождения: ");
        String strBirthdate = sc.nextLine();
        try {
            FormValidator.checkBirthdate(strBirthdate);
        } catch (IncorrectInput incorrectInput) {
            System.out.println(incorrectInput.getMessage());
        }
        System.out.println();

        System.out.println("Введите пол: ");
        String strGender = sc.nextLine();
        try {
            FormValidator.checkGender(strGender);
        } catch (IncorrectInput incorrectInput) {
            System.out.println(incorrectInput.getMessage());
        }
        System.out.println();

        System.out.println("Введите рост: ");
        String strHeight = sc.nextLine();
        try {
            FormValidator.checkHeight(strHeight);
        } catch (IncorrectInput incorrectInput) {
            System.out.println(incorrectInput.getMessage());
        }
    }
}
