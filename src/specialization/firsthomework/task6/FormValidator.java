package specialization.firsthomework.task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

//Фронт со своей стороны не сделал обработку входных данных анкеты!
// Петя очень зол и ему придется написать свои проверки,
// а также кидать исключения, если проверка провалилась.
// Помогите Пете написать класс FormValidator со статическими методами проверки.
// На вход всем методам подается String str.
//a. public void checkName(String str) — длина имени должна быть от 2 до 20 символов, первая буква заглавная.
//b. public void checkBirthdate(String str) — дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты.
//c. public void checkGender(String str) — пол должен корректно матчится в enum Gender, хранящий Male и Female значения.
//d. public void checkHeight(String str) — рост должен быть положительным числом и корректно конвертироваться в double.
public class FormValidator {
    public static void checkName(String strName) throws IncorrectInput {
        if (!strName.matches("^[A-ZА-Я][a-zа-я]{1,20}$")) {
            throw new IncorrectInput("Некорректный ввод имени");
        }
    }

    public static void checkBirthdate(String strDate) throws IncorrectInput {
        try {
            LocalDate x = LocalDate.parse(strDate, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
            LocalDate start = LocalDate.parse("01.01.1900", DateTimeFormatter.ofPattern("dd.MM.yyyy"));
            LocalDate end = LocalDate.now();
            if (x.isBefore(start) || x.isAfter(end)) {
                throw new IncorrectInput("Дата не соответствует допустимому временному периоду");
            }
        } catch (DateTimeParseException e) {
            throw new IncorrectInput("Некорректный ввод даты");
        }
    }

    public static void checkGender(String strGender) throws IncorrectInput {
        try {
            Gender.valueOf(strGender);
        } catch (IllegalArgumentException i) {
            throw new IncorrectInput("Некорректно введён пол");
        }
    }

    public static void checkHeight(String strHeight) throws IncorrectInput {
        try {
            double d = Double.parseDouble(strHeight);
            System.out.println(d);
            if (d <= 0) {
                throw new IncorrectInput("Значение должно быть положительным");
            }
        } catch (NumberFormatException e) {
            throw new IncorrectInput("Некорректно введён рост");
        }
    }
}

