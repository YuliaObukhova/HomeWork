package specialization.firsthomework.RecipeMyOwnException;

public class IngredientsException extends Exception {
    public IngredientsException(String message) {
        super(message);
    }
}
