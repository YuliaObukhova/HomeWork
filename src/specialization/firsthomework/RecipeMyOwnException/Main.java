package specialization.firsthomework.RecipeMyOwnException;

public class Main {
    public static void main(String[] args) throws IngredientsException {
        RecipePasta pastaAlPomodoro = new RecipePasta("Pasta al pomodoro");
        pastaAlPomodoro.buySpaghetti();
        pastaAlPomodoro.buyTomato();

        System.out.println();
        try{
            pastaAlPomodoro.letsCookPasta();
        }
        catch (IngredientsException a){
            System.out.println(a.getMessage());
        }

    }
}
