package specialization.firsthomework.RecipeMyOwnException;

public class RecipePasta {
    private final String name;
    private boolean isThereSpaghetti = false;
    private boolean isThereTomato = false;

    public RecipePasta(String name) {
        this.name = name;
    }

    public void buySpaghetti() {
        this.isThereSpaghetti = true;
        System.out.println("Спагетти куплены");
    }

    public void buyTomato() {
        this.isThereTomato = true;
        System.out.println("Томаты куплены");
    }

    public void letsCookPasta() throws IngredientsException {
        System.out.println("Давайте приготовим пасту " + name + ". Все ли ингридиеты есть у нас? ");
        System.out.println("Спагетти: " + isThereSpaghetti);
        System.out.println("Томаты: " + isThereTomato);

        if (isThereSpaghetti && isThereTomato) {
            System.out.println("Да. Можно начинать");
        } else {
            throw new IngredientsException("Отсутствует один или несколько ингридиентов.");
        }
    }

}
