package specialization.firsthomework.task1;

import java.io.FileNotFoundException;

public class MyCheckedException extends FileNotFoundException {
    public MyCheckedException(String message) {
        super(message);
    }
}
