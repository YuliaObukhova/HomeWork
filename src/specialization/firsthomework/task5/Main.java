package specialization.firsthomework.task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        try {
            inputN();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void inputN() throws Exception {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n < 100 && n > 0) {
            System.out.println("Успешный ввод!");
        } else {
            throw new Exception("Неверный ввод");
        }
    }
}