package specialization.fourthhomework.task3;

import java.util.Arrays;
import java.util.List;

//3. На вход подается список строк.
// Необходимо вывести количество непустых строк в списке.
//Например, для List.of("abc", "", "", "def", "qqq")
// результат равен 3.
public class CounterOfStrings {
    public static void main(String[] args) {
        List<String> arr = Arrays.asList("abc", "", "", "def", "qqq");
        System.out.println(countStrings(arr));
    }

    public static int countStrings(List<String> input) {
        return (int) input.stream()
                .filter(a -> !a.isEmpty()).count();
    }

}
