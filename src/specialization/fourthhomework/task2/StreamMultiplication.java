package specialization.fourthhomework.task2;

import java.util.Arrays;
import java.util.List;

//2. На вход подается список целых чисел.
// Необходимо вывести результат перемножения этих чисел.
//Например, если на вход передали List.of(1, 2, 3, 4, 5),
// то результатом должно
// быть число 120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
public class StreamMultiplication {
    public static void main(String[] args) {
        List<Integer> arr = Arrays.asList(1, 2, 3, 4, 5);
        System.out.println(multiply(arr));
    }

    public static int multiply(List<Integer> input) {
        return input.stream()
                .reduce((a, b) -> a * b)
                .get();
    }
}
