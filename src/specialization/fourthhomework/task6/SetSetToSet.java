package specialization.fourthhomework.task6;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

//Дан Set<Set<Integer>>.
// Необходимо перевести его в Set<Integer>.
public class SetSetToSet {
    public static void main(String[] args) {
        Set<Set<Integer>> a = Set.of(Set.of(1, 10, 100), Set.of(5, 50, 500));
        Set<Integer> b = a.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        b.forEach(System.out::println);
    }
}
