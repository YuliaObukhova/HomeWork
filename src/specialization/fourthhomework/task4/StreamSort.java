package specialization.fourthhomework.task4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

//На вход подается список вещественных чисел.
// Необходимо отсортировать их по убыванию.
public class StreamSort {
    public static void main(String[] args) {
        List<Double> arr = Arrays.asList(15.0,
                56.22,
                0.0,
                -232336.11);

        sortDescending(arr);
    }

    public static void sortDescending(List<Double> input) {
        input.stream()
                .sorted(Comparator.reverseOrder())
                .forEach(System.out::println);
    }
}
