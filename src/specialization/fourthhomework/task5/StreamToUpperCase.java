package specialization.fourthhomework.task5;

import java.util.Arrays;
import java.util.List;

//На вход подается список непустых строк.
// Необходимо привести все символы строк
// к верхнему регистру и вывести их, разделяя запятой.
//Например, для List.of("abc", "def", "qqq")
// результат будет ABC, DEF, QQQ.
public class StreamToUpperCase {
    public static void main(String[] args) {
        List<String> arr = Arrays.asList("abc", "def", "qqq");
        toUpperCase(arr);
    }

    public static void toUpperCase(List<String> input) {
        input.stream().map(String::toUpperCase)
                .forEach(str -> System.out.print(str + ", "));
    }
}
