package specialization.fourthhomework.task1;

import java.util.ArrayList;
import java.util.List;

// Посчитать сумму четных чисел в промежутке от 1 до 100 включительно
// и вывести ее на экран.
public class StreamSum {
    public static void main(String[] args) {

        List<Integer> arr = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            arr.add(i);
        }
        int result = arr.stream()
                .filter((a) -> a % 2 == 0).
                mapToInt(a -> a)
                .sum();

        System.out.println(result);
    }
}
