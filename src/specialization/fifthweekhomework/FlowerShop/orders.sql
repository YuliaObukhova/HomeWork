/*
И, конечно, данные самого заказа тоже нужно как-то хранить! Ирина пока не продумала поля,
но она точно хочет следовать следующим правилам:
● в рамках одного заказа будет продавать только один вид цветов (например, только розы)
● в рамках одного заказа можно купить от 1 до 1000 единиц цветов.
*/
create table orders
(
    order_id           serial primary key,
    order_date_added   timestamp,
    order_customer_id  int references customers (customer_id),
    order_flower_name  varchar references flowersprice (flower_name),
    order_flower_count int check (order_flower_count >= 0 and order_flower_count <= 1000) not null
);

insert into orders(order_date_added, order_customer_id, order_flower_name, order_flower_count)
values (now() - interval '1h', 1, 'Rose', 101),
       (now() - interval '3d', 4, 'Lily', 56),
       (now() - interval '24h', 3, 'Rose', 12),
       (now() - interval '8h', 1, 'Chamomile', 1),
       (now() - interval '7d', 2, 'Rose', 99),
       (now() - interval '1w', 3, 'Chamomile', 678)