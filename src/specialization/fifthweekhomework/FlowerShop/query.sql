-- А также она представляет, какие запросы хочет делать к базе:
-- 1. По идентификатору заказа получить данные заказа и данные клиента,
-- создавшего этот заказ

select o.order_id,
       o.order_date_added,
       c.customer_id,
       customer_name,
       customer_phone_number,
       o.order_flower_name,
       o.order_flower_count,
       o.order_flower_count * f.flower_price as total_price
from orders o
         join
     customers c on o.order_customer_id = c.customer_id
         join
     flowersprice f on o.order_flower_name = f.flower_name
where o.order_id = 5;

-- 2. Получить данные всех заказов одного клиента по идентификатору
-- клиента за последний месяц

select *
from orders o
where o.order_customer_id = 3
  and o.order_date_added >= now() - interval '1 month';

-- 3. Найти заказ с максимальным количеством купленных цветов, вывести их
-- название и количество


select order_flower_count as max,
       flower_name
from orders o
         join
     flowersprice f
     on o.order_flower_name = f.flower_name
         and o.order_flower_count = (select max(order_flower_count) from orders);


-- 4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
-- время

select sum(o.order_flower_count * f.flower_price) as GMV

from orders o
         join
     flowersprice f on o.order_flower_name = f.flower_name;
