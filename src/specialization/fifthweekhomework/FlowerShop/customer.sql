/*
Помимо этого, ей хочется хранить данные своих покупателей
(естественно они дали согласие на хранение персональной информации).
Сохранять нужно Имя и Номер телефона.
 */

create table customers
(
    customer_id           serial primary key,
    customer_name         varchar(30) not null,
    customer_phone_number varchar(30) not null
);

insert into customers (customer_name, customer_phone_number)
values ('Vlad Falevch', '+79671234567'),
       ('Olga Orlova', '+79612345678'),
       ('Ivan Petrov', '+71234567890'),
       ('Anna Sidorova', '+79876543210')


