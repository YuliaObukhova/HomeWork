/*
Ирина, подруга Пети, решила создать свой бизнес по продаже цветов.
Начать она решила с самых основ: создать соответствующую базу данных
для своего бизнеса. Она точно знает,
что будет продавать Розы по 100 золотых монет за единицу,
Лилии по 50 и Ромашки по 25.
 */

create table flowersprice
(
    flower_name  varchar(30) primary key not null,
    flower_price int                     not null
);

insert into flowersprice(flower_name, flower_price)
values ('Rose', '100'),
       ('Lily', '50'),
       ('Chamomile', '25')

