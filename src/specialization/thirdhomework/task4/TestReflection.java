package specialization.thirdhomework.task4;

//4. Написать метод, который с помощью рефлексии получит все интерфейсы класса,
// включая интерфейсы от классов-родителей и интерфейсов-родителей.


import java.util.*;

/**
 * Class A implements Three
 * Class B extends A implements Two
 * Class C extends B implements One
 *
 * interface One
 * interface Two extends One
 *  interface Three extends Two
 */
public class TestReflection {
    public static void main(String[] args) {

        Set<Class<?>> result = getAllInterfaces(B.class);
        for (Class<?> cls : result) {
            System.out.println(cls.getName());
        }
    }

    public static Set<Class<?>> getAllInterfaces(Class<?> cls) {
        Set<Class<?>> interfaces = new HashSet<>();
        Set<Class<?>> parents = new HashSet<>();

        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            for (Class<?> i : interfaces) {
                parents.addAll(Arrays.asList(i.getInterfaces()));
                for (Class<?> y : parents) {
                    parents.addAll(Arrays.asList(y.getInterfaces()));
                }

            }
            cls = cls.getSuperclass();
        }

        Set<Class<?>> allInterfaces = new HashSet<>(interfaces);
        allInterfaces.addAll(parents);

        return allInterfaces;
    }
}
