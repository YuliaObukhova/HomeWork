package specialization.thirdhomework.task1;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)

public @interface IsLike {
    String clinic() default "4 лапы";

    String address() default "Проспект Мира, 4";

    String vet();

    boolean license() default true;
}
