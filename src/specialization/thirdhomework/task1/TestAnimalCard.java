package specialization.thirdhomework.task1;
//Создать аннотацию @IsLike, применимую к классу во время
// выполнения программы. Аннотация может хранить boolean значение.

public class TestAnimalCard {
    public static void main(String[] args) {
        AnimalCard animal = new AnimalCard("Алекс", "Собака", "Джек Рассел Терьер", false);

        IsLike isLike = animal.getClass().getAnnotation(IsLike.class);
        System.out.println("Clinic Name: " + isLike.clinic());
        System.out.println("Address: " + isLike.address());
        System.out.println("Vet: " + isLike.vet());
        System.out.println("License: " + isLike.license());

        animal.getAnimalInfo();
    }
}
