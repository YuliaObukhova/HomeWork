package specialization.thirdhomework.task1;

@IsLike(
        vet = "Иванова А.А."
)
public class AnimalCard {
    private String name;
    private String type;
    private String breed;
    private boolean isItSick;

    public AnimalCard(String name, String type, String breed, boolean isItSick) {
        this.name = name;
        this.type = type;
        this.breed = breed;
        this.isItSick = isItSick;
    }

    public void getAnimalInfo() {
        System.out.println();
        System.out.println("Name: " + name);
        System.out.println("Type: " + type);
        System.out.println("Breed: " + breed);
        System.out.println("Is the animal sick now? " + isItSick);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public boolean isItSick() {
        return isItSick;
    }

    public void setItSick(boolean itSick) {
        isItSick = itSick;
    }
}
