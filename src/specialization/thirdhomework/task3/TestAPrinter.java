package specialization.thirdhomework.task3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


// с помощью рефлексии вызвать метод print() и обработать все возможные ошибки
// (в качестве аргумента передавать любое подходящее число).
// При “ловле” исключений выводить на экран краткое описание ошибки.
public class TestAPrinter {
    public static void main(String[] args) {
        Class<APrinter> clazz = APrinter.class;
        try {
            Constructor<APrinter> constructor = clazz.getDeclaredConstructor();
            APrinter out = constructor.newInstance();
            Method method = clazz.getDeclaredMethod("print", int.class);
            method.invoke(out, 1000);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException |
                 InstantiationException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }
}
