package specialization.thirdhomework.task2;

import specialization.thirdhomework.task1.IsLike;

//Написать метод, который рефлексивно проверит наличие аннотации @IsLike
// на любом переданном классе и выведет значение, хранящееся в аннотации, на экран.
public class TestAnnotationReflexion {
    public static void main(String[] args) {
        printClassDescription(Without.class);
        printClassDescription(With.class);
    }

    public static void printClassDescription(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(IsLike.class)) {
            System.out.println("Класс " + clazz.getName() + " не имеет аннотаций");
            System.out.println();
            return;
        }
        IsLike isLike = clazz.getAnnotation(IsLike.class);
        System.out.println("Класс " + clazz.getName() + " имеет аннотацию:");

        System.out.println("Clinic Name: " + isLike.clinic());
        System.out.println("Address: " + isLike.address());
        System.out.println("Vet: " + isLike.vet());
        System.out.println("License: " + isLike.license());
    }
}
