package basic.firsthomeworksecondpartadd;

import java.util.Scanner;

public class CheckPackage {
    public static void main(String[] args) {
        Scanner p = new Scanner(System.in);
        String mailPackage = p.nextLine();
        boolean kamni = mailPackage.matches("^.*камни!$|^камни!.*$");
        boolean zapr = mailPackage.matches("^.*запрещенная продукция$|^запрещенная продукция.*$");
        boolean notok = mailPackage.matches("^камни!$|^.*запрещенная продукция$|^запрещенная продукция.*$");
        int l = mailPackage.length();

        if (kamni == true && zapr == true) {
            System.out.println("в посылке камни и запрещенная продукция");
        } else {
            if (kamni == true) {
                System.out.println("камни в посылке");
            } else {
                if (zapr == true) {
                    System.out.println("в посылке запрещенная продукция");
                } else {
                    if (l == 0 || notok == false) {
                        System.out.println("все ок");
                    }
                }
            }
        }
    }
}