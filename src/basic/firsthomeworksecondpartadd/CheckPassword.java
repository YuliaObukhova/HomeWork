package basic.firsthomeworksecondpartadd;

import java.util.Scanner;

public class CheckPassword {
    public static void main(String[] args) {
        Scanner p = new Scanner(System.in);
        String password = p.nextLine();

        boolean a = password.matches("^(?=.*[A-Z])(?=.*[_\\*-])(?=.*[0-9])(?=.*[a-z]).{8,}$");
        if (a == true) {
            System.out.println("пароль надежный");
        }
        if (a == false) {
            System.out.println("пароль не прошел проверку");
        }
    }
}