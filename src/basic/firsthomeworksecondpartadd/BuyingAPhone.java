package basic.firsthomeworksecondpartadd;

import java.util.Scanner;

public class BuyingAPhone {
    public static void main(String[] args) {
        Scanner p = new Scanner(System.in);
        String var = p.nextLine();
        int cost = p.nextInt();
        if (50000 <= cost && cost <= 120000) {
            System.out.println(var.matches("^samsung.*$|^iphone.*$") ? "Можно купить" : "Не подходит");
        } else {
            System.out.println("Не подходит");
        }
    }
}
