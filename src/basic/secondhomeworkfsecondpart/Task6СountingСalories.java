package basic.secondhomeworkfsecondpart;

import java.util.Scanner;
//
//6. Подсчет калорий
//        Петя решил начать следить за своей фигурой.
//        Но все существующие приложения для подсчета калорий ему не понравились и он решил написать свое.
//        Петя хочет каждый день записывать сколько белков, жиров, углеводов и калорий он съел, а в конце недели приложение должно его уведомлять, вписался ли он в свою норму или нет.
//
//        На вход подаются числа A — недельная норма белков, B — недельная норма жиров, C — недельная норма углеводов и K — недельная норма калорий.
//        Затем передаются 7 строк, в которых в том же порядке указаны сколько было съедено Петей нутриентов в каждый день недели.
//        Если за неделю в сумме по каждому нутриенту не превышена недельная норма, то вывести “Отлично”, иначе вывести “Нужно есть поменьше”.
//
//        Ограничение:
//        0 < A, B, C < 2000
//        0 < ai, bi, ci < 2000
//        0 < K < 20000
//        0 < ki < 20000

//        Пример входных данных
//        882 595 1232 17500
//        116 85 76 2300
//        100 98 124 2500
//        182 70 154 2750
//        114 85 74 1900
//        96 77 60 1890
//        110 96 98 2500
//        155 67 124 2500
//
//        Пример выходных данных
//        Отлично
public class Task6СountingСalories {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int d = scanner.nextInt();

        final int[] norma = new int[4];
        norma[0] = a;
        norma[1] = b;
        norma[2] = c;
        norma[3] = d;


        int[][] calorieInput = new int[7][4];

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                calorieInput[i][j] = scanner.nextInt();
            }
        }

        int[] weeklyResult = new int[4];

        for (int j = 0; j < 4; j++) {
            int count = 0;
            for (int i = 0; i < 7; i++) {
                count += calorieInput[i][j];
            }
            weeklyResult[j] = count;
        }

        boolean flag = false;
        for (int i = 0; i < 4; i++) {
            if (flag == true) {
                break;
            }
            if (weeklyResult[i] > norma[i]) {
                flag = true;
            }
        }
        System.out.println(flag ? "Нужно есть поменьше" : "Отлично");
    }
}
