package basic.secondhomeworkfsecondpart;
//На вход подается число
// N — количество строк и столбцов матрицы.
// Затем передаются координаты X и Y расположения коня на шахматной доске.
//Необходимо заполнить матрицу размера NxN нулями,
//местоположение коня отметить символом K,
// а позиции, которые он может бить, символом X.

import java.util.Scanner;

public class Task3KnightsMoveInArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//столбцы и строки
        String[][] arr = new String[n][n];
        int y = sc.nextInt();//номер столбца для изменения
        int x = sc.nextInt();//номер строки для изменения

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[j][i] = "0";
            }
        }
        arr[x][y] = "K";
        if (x - 1 > 0 && x - 1 < n && y - 2 > 0 && y - 2 < n) {
            arr[x - 1][y - 2] = "X";//1
        }
        if (x + 1 > 0 && x + 1 < n && y - 2 > 0 && y - 2 < n) {
            arr[x + 1][y - 2] = "X";//2
        }
        if (x - 2 > 0 && x - 2 < n && y - 1 > 0 && y - 1 < n) {
            arr[x - 2][y - 1] = "X";//3
        }
        if (x + 2 > 0 && x + 2 < n && y - 1 > 0 && y - 1 < n) {
            arr[x + 2][y - 1] = "X";//4
        }
        if (x - 2 > 0 && x - 2 < n && y + 1 > 0 && y + 1 < n) {
            arr[x - 2][y + 1] = "X";//5
        }
        if (x + 2 > 0 && x + 2 < n && y + 1 > 0 && y + 1 < n) {
            arr[x + 2][y + 1] = "X";//6
        }
        if (x - 1 > 0 && x - 1 < n && y + 2 > 0 && y + 2 < n) {
            arr[x - 1][y + 2] = "X";//7
        }
        if (x + 1 > 0 && x + 1 < n && y + 2 > 0 && y + 2 < n) {
            arr[x + 1][y + 2] = "X";//8
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j < n - 1) {
                    System.out.print(arr[i][j] + " ");
                } else {
                    System.out.print(arr[i][j]);
                }
            }
            System.out.println();
        }
    }
}
