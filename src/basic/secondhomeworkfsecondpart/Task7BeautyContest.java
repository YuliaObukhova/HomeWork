package basic.secondhomeworkfsecondpart;
////7.Конкурс красоты
//Раз в год Петя проводит конкурс красоты для собак.
//К сожалению, система хранения участников и оценок неудобная,
// а победителя определить надо.
//В первой таблице в системе хранятся имена хозяев,
// во второй - клички животных,
// в третьей — оценки трех судей за выступление каждой собаки.
//Таблицы связаны между собой только по индексу.
//То есть хозяин i-ой собаки указан в i-ой строке первой таблицы,
// а ее оценки — в i-ой строке третьей таблицы.
//Нужно помочь Пете определить топ 3 победителей конкурса.
//
//На вход подается число N — количество участников конкурса.
// Затем в N строках переданы имена хозяев.
//После этого в N строках переданы клички собак.
//Затем передается матрица с N строк,
// 3 вещественных числа в каждой — оценки судей.
//Победителями являются три участника,
// набравшие максимальное среднее арифметическое по оценкам 3 судей.
//Необходимо вывести трех победителей в формате
// “Имя хозяина: кличка, средняя оценка”.
//Среднюю оценку выводить с точностью один знак после запятой.
//
//Гарантируется, что среднее арифметическое для всех участников будет различным.
//
//Ограничение:
//0 < N < 100

import java.util.Scanner;


public class Task7BeautyContest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
//        Object[][] clients = new Object[n][3]; - будущий массив объектов - конкурсантов
        String[][] dogOwner = new String[n][1];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 1; j++) {
                dogOwner[i][j] = sc.next() + ":";

            }
        }
        String[][] dog = new String[n][1];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 1; j++) {
                dog[i][j] = sc.next() + ",";

            }
        }
        double[][] score = new double[n][3];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                score[i][j] = sc.nextInt();
            }
        }

        double[][] middleScore = new double[n][1];
        for (int i = 0; i < n; i++) {
            double middle = ((score[i][0] + score[i][1] + score[i][2]) / 3);
            middleScore[i][0] = (int) (middle * 10) / 10.0;
        }
        Object[][] result = new Object[n][3];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                result[i][0] = dogOwner[i][0];
                result[i][1] = dog[i][0];
                result[i][2] = middleScore[i][0];
            }
        }
        for (int i = 0; i < n; i++) { // Внешний цикл
            for (int j = 0; j < n - 1; j++) {
                double firstInPair = (Double) result[j][2];
                double secondInPair = (Double) result[j + 1][2];
                if (firstInPair < secondInPair) {
                    swap(result, j, j + 1);
                }
            }
        }
        Object[][] winner = new Object[3][3];
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
                winner[j][k] = result[j][k];
            }
        }
        printMatrix(winner);

    }

    /**
     * Обмен строк с индексами i и j местами в двумерном массиве array
     */
    static void swap(Object[][] array, int i, int j) {
        // Обмениваем строки местами
        for (int k = 0; k < array[i].length; k++) { // Итерируемся по элементам двух строк
            // буферный элемент, в котором хранится значение элемента массива строки с индексом i
            Object buff = array[i][k];
            // в ячейку строки с индексом i записываем значение из ячейки строки с индексом j той же колонки
            array[i][k] = array[j][k];
            // в ячейку строки с индексом j записываем значение из буфера
            array[j][k] = buff;
        }
    }


    static void printMatrix(Object[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                if (j < a.length - 1) {
                    System.out.print(a[i][j] + " ");
                } else {
                    System.out.print(a[i][j]);
                }
            }
            System.out.println();
        }
    }
}



