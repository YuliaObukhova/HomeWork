package basic.secondhomeworkfsecondpart;
//На вход подается число N — количество строк и столбцов матрицы.
// Затем передается сама матрица, состоящая из натуральных чисел.
// После этого передается натуральное число P.
// Необходимо найти элемент P в матрице
// и удалить столбец и строку его содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
// Гарантируется, что искомый элемент единственный в массиве.

import java.util.Scanner;

public class Task4RemovalFromTheMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] matrix = new int[n][n];


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();
        int[][] finalmatrix = findElement(matrix, n, p); //массив, длина/ширина массиваб искомый элемент

        for (int i = 0; i < finalmatrix.length; i++) {
            for (int j = 0; j < finalmatrix.length; j++) {
                if (j < finalmatrix.length - 1) {
                    System.out.print(finalmatrix[i][j] + " ");
                } else {
                    System.out.print(finalmatrix[i][j]);
                }
            }
            System.out.println();
        }

    }

    public static int[][] findElement(int[][] oldMatrix, int d, int x) {

        int a = 0; //координата-1 искомого
        int b = 0; //координата-2 искомого
        for (int io = 0; io < d; io++) { //проходим по старой матрице
            for (int jo = 0; jo < d; jo++) {
                if (oldMatrix[io][jo] == x) {
                    a = io;
                    b = jo;
                    break;
                }
            }
        }
        return removeRowAndColumn(oldMatrix, a, b, d);
    }

    public static int[][] removeRowAndColumn(int[][] oldMatrix, int x, int y, int d) {//старая матрица, координат искомого, координат искомого, размер старой матрицы
        int newRow = 0;
        int[][] newMatrix = new int[d - 1][d - 1];
        for (int io = 0; io < d; io++) {
            if (io == x) {
                continue;
            }
            int newColumn = 0;
            for (int jo = 0; jo < d; jo++) {
                if (jo == y) {
                    continue;
                }

                newMatrix[newRow][newColumn] = oldMatrix[io][jo];
                newColumn++;
            }
            newRow++;
        }
        return newMatrix;
    }
}
