package basic.secondhomeworkfsecondpart;

import java.util.Scanner;

public class Task10OutputNumbersRevers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(outputreverse(n));
    }


    public static int outputreverse(int a) {
        if (a < 10) {
            return a;
        }// Шаг рекурсии / рекурсивное условие
        else {
            System.out.print(a % 10 + " ");
            return outputreverse(a / 10);
        }
    }
}

