package basic.secondhomeworkfsecondpart;
//На вход передается N — количество столбцов в двумерном массиве и
// M — количество строк. Затем сам передается двумерный массив,
// состоящий из натуральных чисел.
// Необходимо сохранить в одномерном массиве и
// вывести на экран минимальный элемент каждой строки.


import java.util.Arrays;
import java.util.Scanner;

public class Task1TheMinimumElementOfEachRowInMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(); //- ширина
        int m = scanner.nextInt();//- высота

        //заполняем наш массив
        int[][] a = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
//вывод на экран нашего массива для наглядности
//        for (int[] ints : a) {
//            System.out.println(Arrays.toString(ints));
//        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                Arrays.sort(a[i]);
                System.out.print(a[i][0] + " ");
                break;
            }
            //
        }
    }
}
