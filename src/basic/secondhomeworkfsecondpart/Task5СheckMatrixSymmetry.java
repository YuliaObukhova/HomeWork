package basic.secondhomeworkfsecondpart;

import java.util.Scanner;
//На вход подается число N — количество строк и столбцов матрицы.
// Затем передается сама матрица, состоящая из натуральных чисел.
// Необходимо вывести true,
// если она является симметричной относительно побочной диагонали, false иначе.
// Побочной диагональю называется диагональ,
// проходящая из верхнего правого угла в левый нижний.

//57 190 160 71 42
// 141 79 187 19 71
// 141 16 7 187 160
//        100 42 16 79 190
//        15 100 141 141 57

public class Task5СheckMatrixSymmetry {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }

        int[][] rotatedMatrix = rotateMatrix90(matrix);
        boolean answer = true;
        for (int i = 0; i < n; i++) {
            if (answer == false)
                break;
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    if (rotatedMatrix[i][j] != rotatedMatrix[j][i]) {
                        answer = false;
                        break;
                    }
                }
            }
        }
        System.out.println(answer);
    }
 static int[][] rotateMatrix90(int[][] a) {
                    int[][] b = new int[a.length][a.length];
                    for (int i = 0; i < b.length; i++) {
                        for (int j = 0; j < b.length; j++) {
                            b[j][i] = a[a.length - i - 1][j];
                        }
                    }
                    return b;
                }
        static void printMatrix ( int[][] a){
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < a.length; j++) {
                    System.out.print(a[i][j] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }

}
