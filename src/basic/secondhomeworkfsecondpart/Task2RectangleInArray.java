package basic.secondhomeworkfsecondpart;

//На вход подается число N — количество строк и столбцов матрицы.
// Затем в последующих двух строках подаются
// координаты X (номер столбца) и
// Y (номер строки) точек,
// которые задают прямоугольник.
//        Необходимо отобразить прямоугольник
//        с помощью символа 1 в матрице,
//        заполненной нулями (см. пример)
//        и вывести всю матрицу на экран.
//5
// 1 0
// 4 1

import java.util.Scanner;

public class Task2RectangleInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] matrix = new int[n][n];
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j <= matrix[i].length - 1; j++) {
                matrix[i][j] = 0;
                for (int ab = 0; ab <= (x2 - x1); ab++) {
                    matrix[y1][x1 + ab] = 1;
                }
                for (int cd = 0; cd <= (x2 - x1); cd++) {
                    matrix[y2][x1 + cd] = 1;
                }
                for (int ac = 0; ac <= (y2 - y1); ac++) {
                    matrix[y1 + ac][x1] = 1;
                }
                for (int bd = 0; bd < (y2 - y1); bd++) {
                    matrix[y1 + bd][x2] = 1;
                }

                if (j != matrix[i].length - 1) {
                    System.out.print(matrix[i][j] + " ");
                }
                else {
                    System.out.println(matrix[i][j]);
                }
            }
        }
        System.out.println();
    }
}


