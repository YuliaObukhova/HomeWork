package basic.secondhomeworkfsecondpart;
////
//9. На вход подается число N.
//        Необходимо вывести цифры числа слева направо.
//        Решить задачу нужно через рекурсию.

import java.util.Scanner;

public class Task9OutputNumbers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(output(n));
    }

    public static String output(int a) {

        if (a < 10) {
            return Integer.toString(a);
        } // Шаг рекурсии / рекурсивное условие
        else {
            return output(a / 10) + " " + a % 10;
        }
    }
}
