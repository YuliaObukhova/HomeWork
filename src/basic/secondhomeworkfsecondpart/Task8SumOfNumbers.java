package basic.secondhomeworkfsecondpart;

import java.util.Scanner;

public class Task8SumOfNumbers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(sum(n));
    }
    public static int sum(int a) {
            if (a < 10) {
                return a;
            }
            else {
                return a % 10 + sum(a / 10);
            }
    }
}
