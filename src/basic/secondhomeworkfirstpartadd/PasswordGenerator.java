package basic.secondhomeworkfirstpartadd;

import java.security.SecureRandom;
import java.util.Scanner;

//(2 балла) Создать программу генерирующую пароль.
//На вход подается число N — длина желаемого пароля.
// Необходимо проверить, что N >= 8,
// иначе вывести на экран "Пароль с N количеством символов небезопасен"
// (подставить вместо N число) и предложить
// пользователю еще раз ввести число N.
//Если N >= 8 то сгенерировать пароль,
// удовлетворяющий условиям ниже и вывести его на экран.
//В пароле должны быть:
//● заглавные латинские символы
//● строчные латинские символы
// ● числа
//● специальные знаки(_*-)

public class PasswordGenerator {
    public static void main(String[] args) {
        testPasswordLength();
    }

    /**
     * запрашивает длину желаемого пароля и проверяет ее на соответствие
     * Если соответствует, вызывает метод генерации пароля, передаёт ему длину пароля
     *
     */
    public static void testPasswordLength() {
        System.out.println("Введите число - длинна желаемого пароля: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n >= 8){
            generatePassword(n);
        }
        if (n < 8){
            System.out.println("Пароль с " + n + " количеством символов небезопасен");
            testPasswordLength();
        }
    }
    /**
     * генерирует пароль
     * @param m - длина пароля
     * после генерации пароля вызывает метод, проверяющий пароль,
     *          передаёт ему пароль, длину пароля
     */
    public static void generatePassword(int m) {
        final String symbol = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_*-";
        SecureRandom generator = new SecureRandom(); //генерирует пароль
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < m; i++) {
            int part = generator.nextInt(symbol.length());
            password.append(symbol.charAt(part));
        }
        checkPassword(password.toString(), m);
    }

    /**
     * проверяет пароль на соответствие
     *
     * @param passwordToCheck вариант пароля
     * @param d               длина пароля
     * Если пароль соответсвует, печатает его
     * Если пароль не соответствует, вызывает метод для генерации нового пароля
     */
    public static void checkPassword(String passwordToCheck, int d) {
        boolean a = passwordToCheck.matches("^(?=.*[A-Z])(?=.*[_*-])(?=.*[0-9])(?=.*[a-z]).{8,}$");
        if (a) {
            System.out.println(passwordToCheck);
        }
        if (!a) {
            generatePassword(d);
        }
    }
}
