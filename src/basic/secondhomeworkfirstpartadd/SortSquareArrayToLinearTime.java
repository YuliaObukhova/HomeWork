package basic.secondhomeworkfirstpartadd;
// Необходимо сдать задачу за линейное время:
// На вход подается число N — длина массива.
// Затем передается массив целых чисел (ai) из N элементов,
// отсортированный по возрастанию.
//Необходимо создать массив,
//полученный из исходного возведением в квадрат каждого элемента,
//упорядочить элементы по возрастанию и вывести их на экран.

import java.util.Arrays;
import java.util.Scanner;

public class SortSquareArrayToLinearTime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i = 0; i < n; i++) {
            arr1[i] = scanner.nextInt();
        }
        int[] arr2 = square(arr1, n);
        sort(arr2, n); // сортирует массив, печатает массив
    }

    /**
     * @param Array массив
     * @param m     длина массива
     * @return массив, в котором все элементы возведены в квадрат
     */
    public static int[] square(int[] Array, int m) { //массив, длина массива. возводит элементы массива в квадрат
        for (int i = 0; i < m; i++) {
            Array[i] = Array[i] * Array[i];
        }
        return Array;
    }

    /**
     * @param Array массив
     * @param m     длина массива
     */
    public static void sort(int[] Array, int m) { // сортирует массив, печатает массив
        int i = 0; //pre
        int j = m - 1; //pre
        int[] resultArray = new int[m];
        int f = m - 1;

        do {
            if (Array[i] >= Array[j]) {
                resultArray[f] = Array[i];
                f--;
                i += 1;
            } else {
                resultArray[f] = Array[j];
                f--;
                j -= 1;
            }
        } while (i <= j);
        System.out.println(Arrays.toString(resultArray));
    }
}
