package basic.thirdhomeworkfirstpart.cat;

//Необходимо реализовать класс Cat.
//        У класса должны быть реализованы следующие приватные методы:
//        ● sleep() — выводит на экран “Sleep”
//        ● meow() — выводит на экран “Meow”
//        ● eat() — выводит на экран “Eat”
//        И публичный метод:
//        status() — вызывает один из приватных методов случайным образом.
public class Cat {
    /**
     * выводит на экран “Sleep”
     */
    private void sleep() {
        System.out.println("Sleep");
    }

    /**
     * выводит на экран “Meow”
     */
    private void meow() {
        System.out.println("Meow");
    }

    /**
     * выводит на экран “Eat”
     */
    private void eat() {
        System.out.println("Eat");
    }

    /**
     * вызывает один из приватных методов случайным образом
     */
    public void status() {
        int a = (int) (0 + Math.random() * (2.99 - 0) + 0);
        switch (a) {
            case 0:
                this.sleep();
                break;
            case 1:
                this.meow();
                break;
            case 2:
                this.eat();
                break;
        }
    }
}