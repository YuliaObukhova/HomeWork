package basic.thirdhomeworkfirstpart.dayofweek;

public enum WeekDays {
    MONDAY(1, "Monday"),
    TUESDAY(2, "Tuesday"),
    WEDNESDAY(3, "Wednesday"),
    THURSDAY(4, "Thursday"),
    FRIDAY(5, "Friday"),
    SATURDAY(6, "Saturday"),
    SUNDAY(7, "Sunday");

    private final byte dayNumber;
    private final String name;

    WeekDays(int dayNumber, String name) {
        this.dayNumber = (byte)dayNumber;
        this.name = name;
    }

    public Object getDayNumber() {
        return dayNumber;
    }
    public Object getName(){
        return name;
    }
}