package basic.thirdhomeworkfirstpart.trianglechecker;

public class Main {
    public static void main(String[] args) {
        System.out.println(TriangleChecker.check(1.0, 56.3, 23.7));
        System.out.println(TriangleChecker.check(5.8, 12.9, 7.32));
        System.out.println(TriangleChecker.check(11.78, 24.8, 6.54));
    }
}
