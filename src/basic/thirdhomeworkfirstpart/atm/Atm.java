package basic.thirdhomeworkfirstpart.atm;

// Реализовать класс “банкомат” Atm. Класс должен:
//         ● Содержать конструктор, позволяющий задать курс валют
//         перевода долларов в рубли и курс валют перевода рублей в доллары
//         (можно выбрать и задать любые положительные значения)
//         ● Содержать два публичных метода, которые позволяют переводить
//         переданную сумму рублей в доллары и долларов в рубли
//         ● Хранить приватную переменную счетчик — количество созданных инстансов
//         класса Atm и публичный метод, возвращающий этот счетчик (подсказка: реализуется через static)
public class Atm {
    double rublesPerDollar;
    double dollarsPerRuble;
    private static int count = 0;

    Atm(double rublesPerDollar, double dollarsPerRuble) {
        this.dollarsPerRuble = dollarsPerRuble;
        this.rublesPerDollar = rublesPerDollar;
        count++;

    }

    public double convertDollarsPerRuble(double amount) {
        return dollarsPerRuble * amount;
    }

    public double convertRublesPerDollar(double amount) {
        return rublesPerDollar * amount;
    }

    public static int getCount() {
        return count;
    }
}
