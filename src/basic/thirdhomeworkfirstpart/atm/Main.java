package basic.thirdhomeworkfirstpart.atm;

public class Main {
    public static void main(String[] args) {
        Atm atm1 = new Atm(61.40, 0.016);
        System.out.println(atm1.convertDollarsPerRuble(1));
        System.out.println(atm1.convertRublesPerDollar(1));
        System.out.println(Atm.getCount());

        Atm atm2 = new Atm(61.40, 0.016);
        System.out.println(atm2.convertDollarsPerRuble(10));
        System.out.println(atm2.convertRublesPerDollar(10));
        System.out.println(Atm.getCount());

        Atm atm3 = new Atm(61.40, 0.016);
        System.out.println(atm3.convertDollarsPerRuble(100));
        System.out.println(atm3.convertRublesPerDollar(100));
        System.out.println(Atm.getCount());
    }
}
