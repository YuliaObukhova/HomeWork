package basic.thirdhomeworkfirstpart.studentservice;

import basic.thirdhomeworkfirstpart.student.Student;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Иван", "Иванов", new int[]{10, 10, 10, 9, 10, 8, 10, 9, 8, 10});
        Student student2 = new Student("Петр", "Петров", new int[]{3, 1, 1, 2, 3, 2, 1, 1, 3, 1});
        Student student3 = new Student("Сидор", "Сидоров", new int[]{10, 10, 10, 9, 10, 8, 10, 9, 8, 10});
        Student student4 = new Student("Александр", "Александров", new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});

        Student[] studentsMatrix = new Student[4];
        studentsMatrix[0] = student1;
        studentsMatrix[1] = student2;
        studentsMatrix[2] = student3;
        studentsMatrix[3] = student4;

        System.out.println(StudentService.bestStudent(studentsMatrix).getName() + " "
                + StudentService.bestStudent(studentsMatrix).getSurname());

        System.out.println();

        StudentService.sortStudents(studentsMatrix);
        for (Student matrix : studentsMatrix) {
            System.out.println(matrix.getName() + " " + matrix.getSurname());
        }
    }
}
