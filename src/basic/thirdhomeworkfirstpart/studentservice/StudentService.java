package basic.thirdhomeworkfirstpart.studentservice;

import basic.thirdhomeworkfirstpart.student.Student;
import java.util.Arrays;
import java.util.Comparator;

//Необходимо реализовать класс StudentService.
//У класса должны быть реализованы следующие публичные методы:
// ● bestStudent() — принимает массив студентов (класс Student из предыдущего задания),
// возвращает лучшего студента (т.е. который имеет самый высокий средний балл).
// Если таких несколько — вывести любого.
// ● sortBySurname() — принимает массив студентов
// (класс Student из предыдущего задания) и сортирует его по фамилии.
public class StudentService {
    public StudentService(){
    }
    /**
     * @param matrix массив студентоа
     * @return лучший студент
     */
    public static Student bestStudent(Student[] matrix) {
        boolean isSorted = false;
        Student buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 1; i < matrix.length - 1; i++) {
                if (Student.createMiddleScore(matrix[i].getGrades()) < Student.createMiddleScore(matrix[i + 1].getGrades())) {
                    isSorted = false;
                    buf = matrix[i];
                    matrix[i] = matrix[i + 1];
                    matrix[i + 1] = buf;
                }
            }
        }
        return matrix[0];
    }

    /**
     * @param matrix массив студентов
     *               сортирует массив студентов по фамилии
     */
    public static void sortStudents(Student[] matrix){
        Comparator<Student> bySurname = Comparator.comparing(Student::getSurname);
       Arrays.sort(matrix, bySurname);
    }
}
