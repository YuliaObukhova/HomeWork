package basic.thirdhomeworkfirstpart.timeunit;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
//Необходимо реализовать класс TimeUnit с функционалом, описанным ниже (необходимые поля продумать самостоятельно). Обязательно должны быть реализованы валидации на входные параметры.
// Конструкторы:
// ● Возможность создать TimeUnit, задав часы, минуты и секунды.
// ● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
// должны проставиться нулевыми.
// ● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда должны проставиться нулевыми.
// Публичные методы:
// ● Вывести на экран установленное в классе время в формате hh:mm:ss
// ● Вывести на экран установленное в классе время в 12-часовом формате
// (используя hh:mm:ss am/pm)
// ● Метод, который прибавляет переданное время к установленному в
// TimeUnit (на вход передаются только часы, минуты и секунды).
public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;

    /**
     * TimeUnit, задав часы, минуты и секунды
     * @param hours  - часы
     * @param minutes - минуты
     * @param seconds - секунды
     */
    TimeUnit(int hours, int minutes, int seconds) {
        if (validateHours(hours)) {
            if (validateMinutesAndSeconds(minutes)) {
                if (validateMinutesAndSeconds(seconds)) {
                    this.hours = hours;
                    this.minutes = minutes;
                    this.seconds = seconds;
                }
            }
        }
    }

    /** TimeUnit, задав часы и минуты.
     * @param hours часы
     * @param minutes минуты
     */
    TimeUnit(int hours, int minutes) {
        if (validateHours(hours)) {


            if (validateMinutesAndSeconds(minutes)) {
                this.hours = hours;
                this.minutes = minutes;
                this.seconds = 0;
            }
        }

    }

    /** TimeUnit задав часы.
     * @param hours часы
     */
    TimeUnit(int hours) {
        if (validateHours(hours)) {
            this.hours = hours;
            this.minutes = 0;
            this.seconds = 0;
        }
    }

    public static boolean validateHours(int a) {
        return 0 <= a && a < 24;
    }
    public static boolean validateMinutesAndSeconds(int a) {
        return 0 <= a && a < 60;
    }

    /** Выводит на экран установленное в классе время в формате hh:mm:ss
     * @param TimeUnit объект
     */
    public static void timeIs(TimeUnit TimeUnit) {
        long timeInMills = (((long)TimeUnit.hours * 3600000) + ((long)TimeUnit.minutes * 60000) + ((long)TimeUnit.seconds * 1000));

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String timeString = timeFormat.format(new Date(timeInMills));
        System.out.println(timeString);
    }

    /**
     * Выводит на экран установленное в классе время в 12-часовом формате
     * // (используя hh:mm:ss am/pm)
     * @param TimeUnit объект
     */
    public static void timeIsAmPm(TimeUnit TimeUnit){
      long timeInMills = (((long)TimeUnit.hours * 3600000) + ((long)TimeUnit.minutes * 60000) + ((long)TimeUnit.seconds * 1000));

        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss a");
        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String timeString = timeFormat.format(new Date(timeInMills));
        System.out.println(timeString);

    }

    /**
     * прибавляет переданное время к установленному в
     * TimeUnit (на вход передаются только часы, минуты и секунды).
     * @param h прибавляемые часы
     * @param min прибавляемые минуты
     * @param sec прибавляемые секунды
     */
    public void addTime(int h, int min, int sec){
        int resultHours = hours + h;
        int resultMinutes = minutes + min;
        int resultSeconds = seconds + sec;

        long timeInMills = (((long)resultHours * 3600000) + ((long)resultMinutes * 60000) + ((long)resultSeconds * 1000));
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String timeString = timeFormat.format(new Date(timeInMills));
        System.out.println(timeString);
    }
}


