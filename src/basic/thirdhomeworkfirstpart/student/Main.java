package basic.thirdhomeworkfirstpart.student;



public class Main {
    public static void main(String[] args) {
        Student student1 = new Student("Иван", "Иванов", new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        student1.addScore(5, student1.getGrades());
        System.out.println(student1.getName() + " " + student1.getSurname() + " " + Student.createMiddleScore(student1.getGrades()));
    }
}
