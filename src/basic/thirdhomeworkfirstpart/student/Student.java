package basic.thirdhomeworkfirstpart.student;

//Необходимо реализовать класс Student
// У класса должны быть следующие приватные поля:
//   ● String name — имя студента
//   ● String surname — фамилия студента
//   ● int[] grades — последние 10 оценок студента.
//   Их может быть меньше, но
//   не может быть больше 10. И следующие публичные методы:
//   ● геттер/сеттер для name
//   ● геттер/сеттер для surname
//   ● геттер/сеттер для grades
//   ● метод, добавляющий новую оценку в grades. Самая первая оценка
//   должна быть удалена, новая должна сохраниться в конце массива (т.е.
//   массив должен сдвинуться на 1 влево).
//   ● метод, возвращающий средний балл студента (рассчитывается как
//   среднее арифметическое от всех оценок в массиве grades)
public class Student {
        private String name;
        private String surname;
        private int[] grades;
        private double middleScore;


 public Student(String name, String surname, int[] grades) {
     this.name = name;
     this.surname = surname;
     this.grades = grades;
 }

    public String getName () {
        return name;
    }

    public void setName (String newName){
        name = newName;
    }

    public String getSurname () {
        return surname;
    }

    public void setSurname (String newSurname){
        surname = newSurname;
    }

    public int[] getGrades () {
        return grades;
    }

    public void setGrades ( int[] newGrades){
        grades = newGrades;
    }

    public double getMiddleScore() {
        return middleScore;
    }

    public void setMiddleScore(int[] grades) {
        this.middleScore = createMiddleScore(getGrades());
    }

    /**
         * @param score  - новая оценка
         * @param grades - прошлые оценки
         */
        public void addScore ( int score, int[] grades){
            int[] array1 = new int[grades.length + 1];
            int count = 0;
            for (int i = 0; i < grades.length; i++) {
                array1[i] = grades[i];
                array1[grades.length] = score;
                count++;
            }
            if (array1.length > 10) {
                int[] array2 = new int[10];
                count = 0;
                for (int i = 0; i < array2.length; i++) {
                    array2[i] = array1[count + 1];
                    count++;
                }
                setGrades(array2);
            }
            setGrades(array1);
        }

    /**
     * @param array - все оценки студента
     * @return средний балл студента
     */
    public static double createMiddleScore(int[] array) {
        double middleScore;
        double sum = 0;
        int count = 0;
        for (int j : array) {
            sum += j;
            count++;
        }
        middleScore = (int) (((sum / count) * 10) / 10);
        return middleScore;
    }
}
