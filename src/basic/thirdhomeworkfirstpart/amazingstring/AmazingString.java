package basic.thirdhomeworkfirstpart.amazingstring;

import java.util.Arrays;

//Необходимо реализовать класс AmazingString, который хранит внутри
// себя строку как массив char и предоставляет следующий функционал:
// Конструкторы:
//        ● Создание AmazingString, принимая на вход массив char
//        ● Создание AmazingString, принимая на вход String
//Публичные методы (названия методов, входные и выходные
//параметры продумать самостоятельно).
//Все методы ниже нужно реализовать “руками”, т.е. не прибегая
//к переводу массива char в String и без использования стандартных
// методов класса String.
//        ● Вернуть i-ый символ строки
//        ● Вернуть длину строки
//        ● Вывести строку на экран
//        ● Проверить, есть ли переданная подстрока в AmazingString (на вход
//        подается массив char). Вернуть true, если найдена и false иначе
//        ● Проверить, есть ли переданная подстрока в AmazingString (на вход
//        подается String). Вернуть true, если найдена и false иначе
//        ● Удалить из строки AmazingString ведущие пробельные символы, если
//        они есть
//        ● Развернуть строку (первый символ должен стать последним, а
//        последний первым и т.д.)
public class AmazingString {
    private char[] charArray;

    AmazingString(char[] arr) {
        this.charArray = arr;
    }

    AmazingString(String message) {
        this.charArray = message.toCharArray();
    }

    public char indexIMean(int i) {
        return charArray[i];
    }

    public int lengthOfString() {
        return charArray.length;
    }

    public void printString() {
        for (char c : charArray) {
            System.out.print(c);
        }
    }

    public boolean checkContainChar(char[] subCh) {
        boolean contain = false;
        for (int i = 0; i < charArray.length; i++) {
            for (char ch : subCh) {
                if (charArray[i] == ch && subCh.length <= charArray.length - i) {
                    char[] var = new char[subCh.length];
                    System.arraycopy(charArray, i, var, 0, subCh.length);
                    if (Arrays.equals(var, subCh)) {
                        contain = true;
                    }
                }
            }
        }

        return contain;
    }

    public boolean checkContainString(String subStr) {
        char[] chars = new char[subStr.length()];
        for (int i = 0; i < subStr.length(); i++) {
            chars[i] = subStr.charAt(i);
        }
        return checkContainChar(chars);
    }

    public void trimSpace() {
        for (char c : charArray)
            if (c != ' ') {
                System.out.print(c);
            }
    }

    public void reverse() {
        for (int i = charArray.length - 1; i >= 0; i--) {
            System.out.print(charArray[i]);
        }
    }

}

