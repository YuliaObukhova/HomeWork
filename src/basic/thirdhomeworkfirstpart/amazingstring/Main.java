package basic.thirdhomeworkfirstpart.amazingstring;

public class Main {
    public static void main(String[] args) {
        AmazingString a = new AmazingString(new char[]{' ', 'G', 'o', 'o', 'd', ' ', 'D', 'a', 'y'});
        AmazingString b = new AmazingString("Java is fun!");

        System.out.println(a.indexIMean(1));
        System.out.println(b.indexIMean(1));
        System.out.println(a.lengthOfString());
        System.out.println(b.lengthOfString());
        a.printString();
        System.out.println();
        b.printString();
        System.out.println();
        System.out.println(a.checkContainChar(new char[]{'o', 'd'}));
        System.out.println(b.checkContainChar(new char[]{'J', 'a', 'r', 'a'}));
        System.out.println(a.checkContainString("ok"));
        System.out.println(b.checkContainString("fun"));
        a.trimSpace();
        System.out.println();
        b.trimSpace();
        System.out.println();
        a.reverse();
        System.out.println();
        b.reverse();
    }
}
