package basic.firsthomeworkfirstpart;

import java.util.Scanner;

public class InchesToCMConverter {
    public static void main(String[] args) {
        double i, cm;

        Scanner input = new Scanner(System.in);
        i = input.nextDouble();

        cm = i * 2.54;

        System.out.println(cm);

    }
}