package basic.firsthomeworkfirstpart;

import java.util.Scanner;

public class KilometersToMiles {
    public static void main(String[] args) {
        double count, mi;

        Scanner input = new Scanner(System.in);
        count = input.nextDouble();
        mi = count / 1.60934;
        System.out.println(mi);

    }
}