package basic.firsthomeworkfirstpart;

import java.util.Scanner;

public class SecondsToTime {
    public static void main(String[] args) {
        int count;
        int h;
        int m;
        int fm;

        Scanner input = new Scanner(System.in);
        count = input.nextInt();

        m = count / 60;
        h = m / 60;
        fm = m - (h * 60);

        System.out.println(h + " " + fm);
    }
}
