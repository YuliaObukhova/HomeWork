package basic.firsthomeworkfirstpart;

import java.util.Scanner;

public class RootMeanSqare {
    public static void main(String[] args) {
        double a, b, c;

        Scanner input = new Scanner(System.in);
        a = input.nextDouble();
        b = input.nextDouble();
        c = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2);
        System.out.println(c);
    }
}
