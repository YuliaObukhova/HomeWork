package basic.secondhomeworkfirstpart;
//5. Сдвиг
//        На вход подается число N — длина массива.
//        Затем передается массив целых чисел (ai) из N элементов.
//        После этого передается число M — величина сдвига.
//        Необходимо циклически сдвинуть элементы массива на M элементов вправо.
//        Пример входных данных
//        5
//        38 44 0 -11 2
//        2
//        Пример выходных данных
//        -11 2 38 44 0

import java.util.Scanner;

public class Displacement {
    public static void main(String[] args) {
        Scanner scanner = new Scanner((System.in));
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();

        displacenmentElements(n, arr, m);
    }

    static void displacenmentElements(int a, int[] Array, int b) { // длина массива, массив, количество итераций(шаги сдвига)
        for (; b > 0; b--) {
            int lastIndex = Array[Array.length - 1];
            for (int i = Array.length - 2; i >= 0; i--) { // i - индекс
                Array[i + 1] = Array[i];
            }
            Array[0] = lastIndex;
        }
        if (b == 0) {
            for (int k : Array) {
                System.out.print(k + " ");
            }
        }
    }
}

