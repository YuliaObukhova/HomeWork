package basic.secondhomeworkfirstpart;

import java.util.Arrays;
import java.util.Scanner;

//2. (1 балл) На вход подается число N — длина массива.
// Затем передается массив целых чисел (ai) из N элементов.
// После этого аналогично передается второй массив (aj) длины M.
//Необходимо вывести на экран true, если два массива одинаковы
// (то есть содержат одинаковое количество элементов
// и для каждого i == j элемент ai == aj). Иначе вывести false.
public class ArrayEquality {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < ai.length; i++) {
            ai[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();
        int[] aj = new int[m];
        for (int i = 0; i < aj.length; i++) {
            aj[i] = scanner.nextInt();
        }
        System.out.println(Arrays.equals(ai, aj));
    }
}
