package basic.secondhomeworkfirstpart;

import java.util.Arrays;
import java.util.Scanner;
//7. (1 балл) На вход подается число N — длина массива.
// Затем передается массив целых чисел (ai) из N элементов,
// отсортированный по возрастанию.
//Необходимо создать массив,
// полученный из исходного возведением в квадрат каждого элемента,
// упорядочить элементы по возрастанию и вывести их на экран.
public class Squaring {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] arr1 = new double[n];
        for (int i = 0; i < n; i++) {
            arr1[i] = scanner.nextInt();
        }
        squaring(arr1, n);
        Arrays.sort(arr1);
        printbysymbols(arr1);
    }

    public static double[] squaring(double[] Array, int a) { //массив, длина массива, возводит каждый элемент в квадрат, возвращает несортированный массив
        for (int i = 0; i < a; i++) {
            Array[i] = Math.pow(Array[i], 2);
        }
        return Array;
    }

    public static void printbysymbols(double[] Array) {
        for (double a : Array) {
            System.out.print((int) a + " ");
        }
    }
}
