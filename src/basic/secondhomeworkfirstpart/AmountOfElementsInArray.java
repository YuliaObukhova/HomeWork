package basic.secondhomeworkfirstpart;
//4. Количество различных элементов
//На вход подается число N — длина массива.
//Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.
//
//Необходимо вывести на экран построчно сколько встретилось различных элементов.
//Каждая строка должна содержать количество элементов и сам элемент через пробел.

import java.util.Scanner;

//
//Пример входных данных
//6
//7 7 7 10 26 26
//
//
//Пример выходных данных
//3 7
//1 10
//2 26
public class AmountOfElementsInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
        }
        scanner.close();
        amountOfElements(arr);
    }

     static void amountOfElements(int[] Array) {
        int count = 1;
        for (int a = 0; a < Array.length; a++) {
            for (int j = 1; j < Array.length; j++) {

                if (Array[a] == Array[j])
                    count++;

            }
            System.out.println(count + " " + Array[a]);
            int[] Array1 = new int[Array.length - count];
            System.arraycopy(Array, count, Array1, 0, Array.length - count);
            if (Array.length == count) {
                System.exit(0);
            } else {
                amountOfElements(Array1);
            }
        }
    }

}
