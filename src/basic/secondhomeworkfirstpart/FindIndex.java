package basic.secondhomeworkfirstpart;
//3. Найти индекс
//На вход подается число N — длина массива.
//Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.
//После этого вводится число X — элемент, который нужно добавить в массив, чтобы сортировка в массиве сохранилась.
//
//
//Необходимо вывести на экран индекс элемента массива, куда нужно добавить X.
//Если в массиве уже есть число равное X, то X нужно поставить после уже существующего.

//Пример входных данных
//6
//10 20 30 40 45 60
//12
//Пример выходных данных
//        1

import java.util.Arrays;
import java.util.Scanner;

public class FindIndex {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < ai.length; i++) {
            ai[i] = scanner.nextInt();
        }

        int x = scanner.nextInt();
        int[] aj = new int[n + 1];
        System.arraycopy(ai, 0, aj, 0, n);
        aj[aj.length - 1] = x;
        Arrays.sort(aj);
        System.out.println(linearSearch(aj, x));
    }

    public static int linearSearch(int[] arr, int elementToSearch) {
        for (int index = arr.length - 1; index < arr.length; index--) {
            if (arr[index] == elementToSearch)
                return index;
        }
        return -1;
    }

}