package basic.secondhomeworkfirstpart;

import java.util.Random;
import java.util.Scanner;

//10.(1 балл) Необходимо реализовать игру.
// Алгоритм игры должен быть записан в отдельном методе.
// В методе main должен быть только вызов метода с алгоритмом игры.
//Условия следующие:
//Компьютер «загадывает» (с помощью генератора случайных чисел)
// целое число M в промежутке от 0 до 1000 включительно.
// Затем предлагает пользователю угадать это число.
// Пользователь вводит число с клавиатуры.
// Если пользователь угадал число M, то вывести на экран "Победа!".
// Если введенное пользователем число меньше M, то вывести на экран
// "Это число меньше загаданного." Если введенное число больше,
// то вывести "Это число больше загаданного."
// Продолжать игру до тех пор, пока число не будет отгадано
// или пока не будет введено любое отрицательное число.

public class GameGuessNumber {
    public static void main(String[] args) {
        Random generator = new Random();
        int m = generator.nextInt(1001);
        gameGuessNumber(m);
    }

    /**
     * предлагает угадить число,
     * указывает, является ли число большим/меньшим, чем загаданное,
     * сообщает в случае верного ответа
     */
    public static void gameGuessNumber(int m) {

        System.out.println("Угадайте загаданное число от 1 до 1000: ");
        Scanner scanner = new Scanner(System.in);
        int guess = scanner.nextInt();
        if (guess >= 0) {
            if (m != guess) {
                if (guess < m) {
                    System.out.println("Это число меньше загаданного.");
                    gameGuessNumber(m);
                }
                if (guess > m) {
                    System.out.println("Это число больше загаданного.");
                    gameGuessNumber(m);
                }
            }
            if (m == guess) {
                System.out.println("Победа!");
                System.exit(1);
            }
        } else {
            System.exit(1);
        }
    }
}
