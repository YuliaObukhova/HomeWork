package basic.secondhomeworkfirstpart;
//Найти ближайшее
//        На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов.
//        После этого передается число M.
//
//        Необходимо найти в массиве число, максимально близкое к M (т.е. такое число, для которого |ai - M| минимальное).
//        Если их несколько, то вывести максимальное число.

import java.util.Scanner;

public class FindNearest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];


        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        System.out.println(findnearest(arr, m));
    }

    public static int findnearest(int[] Array, int b) { //массив; число, к которому нужно найти ближайшее
        int min = Math.abs(Array[0] - b);
        int index = Array[0];
        for (int i = 1; i < Array.length; i++) {
            if (Math.abs(Array[i] - b) < min) {
                min = Math.abs(Array[i] - b);
                index = Array[i];
            }
            if (Math.abs(Array[i] - b) > min) {
                continue;
            }
            if(Math.abs(Array[i] - b) == min) {
                index = Array[i];
            }
        } return index;
    }
}
