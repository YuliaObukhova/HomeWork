package basic.secondhomeworkfirstpart;
//1. (1 балл) На вход подается число N — длина массива.
// Затем передается массив вещественных чисел (ai) из N элементов.
//Необходимо реализовать метод, который принимает на вход
// полученный массив и возвращает среднее арифметическое всех чисел
// массива. Вывести среднее арифметическое на экран.
import java.util.Scanner;
// 1,5 2,7 3,14
public class TheArithmeticMeanArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] arr = new double[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextDouble();
        }
        double sum = 0;
        for (int j = 0; j < n ; j++) {
            sum += arr[j];
        }
        double average = sum / n;
        System.out.println(average);
    }
}
