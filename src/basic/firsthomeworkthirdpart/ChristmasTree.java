package basic.firsthomeworkthirdpart;
//Вывести на экран “ёлочку” из символа звездочки (*)
// заданной высоты N. На N + 1 строке у “ёлочки” должен
// быть отображен ствол из символа |
import java.util.Scanner;


public class ChristmasTree {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();//делает на 1 пробел меньше количества строк (по картинке задания условие
        int prob = n - 1;
        int resh = 1;
        for (int i = 1; i <= n; i++) { //цикл для каждого уровня веток
            for (int j = 0; j < prob; j++) { // j считает количество пробелов в строке
                System.out.print(" ");
            }
            for (int j = 0; j < resh; j++) {
                System.out.print("#");
            }
            prob--;
            resh = resh + 2;
            System.out.println();
        }
        for (int a = 0; a < resh/2-1; a++){
            System.out.print(" ");
        }
        System.out.println("|");
    }
}
