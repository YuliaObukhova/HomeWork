package basic.firsthomeworkthirdpart;

import java.util.Scanner;

public class CalculateTheExpression {
    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        int x = a.nextInt();
        int y = a.nextInt();
        int sum = 0;

        for (int i = 1; i <= y; i++){
            sum += Math.pow(x, i);
        }
        System.out.println(sum);
    }
}

