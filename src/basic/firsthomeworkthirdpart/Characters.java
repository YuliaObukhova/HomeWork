package basic.firsthomeworkthirdpart;

import java.util.Scanner;

public class Characters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        int count = 0;

        for (int i = 0; i < a.length(); i++){
             count++;
        }
        int spaces = a.length() - a.replace(" ", "").length();
        System.out.println(count - spaces);
    }
}
