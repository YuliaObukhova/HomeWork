package basic.firsthomeworkthirdpart;
//На вход последовательно подается возрастающая последовательность из n целых чисел, которая может начинаться с отрицательного числа.
//        Посчитать и вывести на экран, какое количество отрицательных чисел было введено в начале последовательности. Помимо этого нужно прекратить выполнение цикла при получении первого неотрицательного числа на вход.

import java.util.Scanner;

public class CountOfNegativeNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int count = 0;
        for (int i = 0; n < 0; i++) {
            count++;
            n = scanner.nextInt();
            if (n > 0) {
                break;
            }
        }
        System.out.println(count);
    }
}
