package basic.firsthomeworkthirdpart;
//8. На вход подается:
//        ○ целое число n,
//        Входные данные
//        Выходные данные
//        Hello world
//        10
//        Never give up
//        11
//        Java 12 Базовый модуль Неделя 2 ДЗ 1 Часть 2
//        ○ целое число p
//        ○ целые числа a1, a2 , ... an
//        Необходимо вычислить сумму всех чисел a1, a2, a3 ... an которые строго больше p.
//        Ограничения:
//        0 < m, n, ai < 1000

import java.util.Scanner;

public class CalculateTheSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int sum = 0;
        for (int k : arr) {
            if (k > p) {
                sum += k;
            }
        }
        System.out.println(sum);
    }
}
