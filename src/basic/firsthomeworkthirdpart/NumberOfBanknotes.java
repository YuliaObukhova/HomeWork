package basic.firsthomeworkthirdpart;

import java.util.Scanner;
// 8 4 2 1
public class NumberOfBanknotes {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int totalAmount = scanner.nextInt();
        int eight = totalAmount / 8;
        System.out.print(eight + " ");
        int four = (totalAmount - (eight * 8)) / 4;
        System.out.print(four + " ");
        int two = (totalAmount - (four * 4) - (eight * 8)) / 2;
        System.out.print(two + " ");
        int one = (totalAmount - (four * 4) - (eight * 8) - (two * 2)) / 1;
        System.out.println(one);
    }
}
