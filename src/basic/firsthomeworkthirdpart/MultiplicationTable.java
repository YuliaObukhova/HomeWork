package basic.firsthomeworkthirdpart;

public class MultiplicationTable {
    public static void main(String[] args) {

        for (int j = 1; j < 10; j++) {
            for (int i = 1; i < 10; i++) {
                int sum = 0;
                sum += i * j;
                System.out.println(j + " x " + i + " = " + sum);
            }
        }
    }
}
