package basic.firsthomeworksecondpart;

import java.util.Scanner;

public class EnglishLevel {
    public static void main(String[] args) {
        int count;

        Scanner input = new Scanner(System.in);
        count = input.nextInt();

        if (count < 500) {
            System.out.println("beginner");
        }
        if (500 <= count && count < 1500) {
            System.out.println("pre-intermediate");
        }
        if (1500 <= count && count < 2500) {
            System.out.println("intermediate");
        }
        if (2500 <= count && count < 3500) {
            System.out.println("upper-intermediate");
        }
        if (count >= 3500 && true) {
            System.out.println("fluent");
        }
    }
}
