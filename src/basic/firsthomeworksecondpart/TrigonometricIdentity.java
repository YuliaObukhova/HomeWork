package basic.firsthomeworksecondpart;

import java.util.Scanner;

public class TrigonometricIdentity {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        double x = input.nextDouble();

        double a = (int) (Math.pow(Math.sin(x), 2) + Math.pow(Math.cos(x), 2) - 1);


        if (a == 0) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}