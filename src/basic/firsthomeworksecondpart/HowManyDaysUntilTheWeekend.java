package basic.firsthomeworksecondpart;

import java.util.Scanner;

public class HowManyDaysUntilTheWeekend {
    public static void main(String[] args) {
        int n, d;

        Scanner input = new Scanner(System.in);
        n = input.nextInt();
        if (n >= 6) {
            System.out.println("Ура, выходные!");
        } else {
            d = 6 - n;
            System.out.println(d);
        }
    }
}