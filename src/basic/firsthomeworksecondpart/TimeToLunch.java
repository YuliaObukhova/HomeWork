package basic.firsthomeworksecondpart;

import java.util.Scanner;

public class TimeToLunch {
    public static void main(String[] args) {
        int t;

        Scanner input = new Scanner(System.in);
        t = input.nextInt();
        if (t > 12) {
            System.out.println("Пора");
        } else {
            System.out.println("Рано");
        }
    }
}
