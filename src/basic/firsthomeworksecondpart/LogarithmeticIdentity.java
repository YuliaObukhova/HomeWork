package basic.firsthomeworksecondpart;

import java.util.Scanner;

public class LogarithmeticIdentity {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        double n = input.nextDouble();
        double b = Math.log(Math.pow(Math.E, n)) - n;

        if (b == 0) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}