package basic.firsthomeworksecondpart;

import java.util.Scanner;

public class StringUpToLastSpace {
        public static void main(String[] args) {

                String s, s1, s2;
                int p;

                Scanner input = new Scanner(System.in);
                s = input.nextLine();
                p = s.lastIndexOf(' ');
                s1 = s.substring(0, p);
                s2 = s.substring(p + 1);

                System.out.println(s1);
                System.out.println(s2);
        }
}
