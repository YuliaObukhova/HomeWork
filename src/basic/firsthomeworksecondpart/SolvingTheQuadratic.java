package basic.firsthomeworksecondpart;

import java.util.Scanner;

public class SolvingTheQuadratic {
    public static void main(String[] args) {
        int a, b, c, dis;

        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();
        dis = (b * b) - (4 * a * c);

        if (dis < 0) {
            System.out.println("Решения нет");
        } else {
            System.out.println("Решение есть");
        }
    }
}