package basic.firsthomeworksecondpart;

import java.util.Scanner;

public class MakeATriangle {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();

        if (a < b + c && b < a + c && c < a + b) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
