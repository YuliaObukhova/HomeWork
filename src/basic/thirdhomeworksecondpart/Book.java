package basic.thirdhomeworksecondpart;

import java.util.ArrayList;
import java.util.Objects;

public class Book {
    private final String name;
    private final String author;
    private boolean isTaken;
    private ArrayList<Integer> ratingList;
    private double middleScore;

    Book(String name, String author) {
        this.name = name;
        this.author = author;
        this.isTaken = false;
        this.middleScore = getMiddleScore();
        ratingList = new ArrayList<>();
    }

    public double rating() {
        double sum = 0;
        int count = 0;
        for (Integer integer : ratingList) {
            sum += integer;
            count++;

        }
        return middleScore = sum / count;
    }

    public void addScore(int score){
        this.ratingList.add(score);
    }

    public double getMiddleScore() {
        return middleScore;
    }

    public boolean isTaken() {
        return isTaken;
    }

    public void setTaken(boolean taken) {
        isTaken = taken;
    }

    public String getName() {
        return name;
    }
    public String getAuthor() {
        return author;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Book book = (Book) obj;
        boolean isEquals = false;
        if (Objects.equals(name, book.name) || (name != null && name.equals(book.getName()))){
                if (Objects.equals(author, book.author) || (author != null && author.equals(book.getAuthor()))){
                isEquals = true;
            }
        }
        return isEquals;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, author);
    }
}
