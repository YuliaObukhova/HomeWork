package basic.thirdhomeworksecondpart;

import java.util.ArrayList;

public class Library {
    private final ArrayList<Book> library;

    Library() {
        this.library = new ArrayList<>();
    }

    private int count = 1;

    /**
     * Добавляет книгу в библиотеку, если в библиотеке ещё нет такой книги
     *
     * @param book новая книга
     */
    public void addBookInLibrary(Book book) {
        boolean flag = false;
        for (Book value : library) {
            if (!book.equals(value)) {
                continue;
            }
            if (value.equals(book)) {
                flag = true;
                System.out.println("Такая книга уже есть в библиотеке");
                break;
            }
        }
        if (!flag) {
            library.add(book);
            System.out.println("Книга " + book.getName() + " " + book.getAuthor() + " добавлена в библиотеку");

        }
    }

    /**
     * Выводит список книг в библиотеке
     */
    public void printLibrary() {
        for (Book book : library) {
            System.out.println(book.getName() + " " + book.getAuthor());
        }
    }

    /**
     * удаляет книгу из библиотеки по названию
     *
     * @param s название книги
     */
    public void deleteBook(String s) {
        for (Book book : library) {
            if (book.getName().equals(s) && book.isTaken()) {
                System.out.println("Книга у посетителя");
                break;
            }
            if (book.getName().equals(s) && !book.isTaken()) {
                library.remove(book);
                System.out.println("Книга " + book.getName() + " " + book.getAuthor() + " удалена из библиотеки");
                break;
            }
        }
    }


    /**
     * Находит и возвращает книгу по названию
     *
     * @param s название книги, которую нужно найти
     * @return найденная книга
     */
    public Book findBook(String s) {
        for (Book book : library) {
            if (book.getName().equals(s)) {
                return book;

            }

        }
        return null;
    }

    /**
     * найти и вернуть список книг автора
     *
     * @param s имя автора
     * @return список книг автора
     */
    public ArrayList<Book> findByAuthor(String s) {
        System.out.println("Книги автора " + s + ":");
        ArrayList<Book> authorsBookList = new ArrayList<>(library.size());
        for (Book book : library) {
            if (book.getAuthor().equals(s)) {
                System.out.println(book.getName());
                authorsBookList.add(book);
            }
        }
        return authorsBookList;
    }

    /** выдаёт книгу посетителю
     * @param book книга
     * @param visitor посетитель
     */
    public void lendABook(Book book, Visitor visitor) {
        boolean flag = false;
        for (Book value : library) {
            if (!book.equals(value)) {
                continue;
            }
            if (value.equals(book)) {
                flag = true;
                break;
            }
        }
        if (flag) {
            if (!visitor.isAlreadyGotTheBook()) {
                if (!book.isTaken()) {
                    book.setTaken(true);
                    visitor.setAlreadyGotTheBook(true);
                    if (visitor.getId() == null) {
                        visitor.setId("id" + count);
                        count++;
                    }
                    visitor.setTakenBook(book);
                    System.out.println(visitor.getName() + " " + visitor.getId() + " взял книгу " + book.getName() + " " + book.getAuthor());
                    } else {
                    System.out.println("Книга взята другим посетителем");
                }
            } else {
                System.out.println("У посетителя уже есть книга");
            }
        } else {
            System.out.println("Такой книги нет в библиотеке");
        }
    }

    /** Реализует позврат книги посетителем
     * @param book книга
     * @param visitor посетитель
     * @param score оценка книги посетителем
     */
    public void returnBook(Book book, Visitor visitor, int score) {
        boolean flag = false;
        for (Book value : library) {
            if (!book.equals(value)) {//проверяет есть ли такая книга в библиотеке
                continue;
            }
            if (value.equals(book)) {
                flag = true;
                break;
            }
        }
        if (flag) {
            if (visitor.isAlreadyGotTheBook()){
                if (book.isTaken()) {
                    if (visitor.getTakenBook().equals(book)) {
                        book.setTaken(false);
                        visitor.setAlreadyGotTheBook(false);
//                        visitor.setTakenBook();
                        book.addScore(score);
                        System.out.println(visitor.getName() + " " + visitor.getId() + " вернул книгу " + book.getName() + " " + book.getAuthor());
                    } else {
                        System.out.println("Посетитель не брал эту книгу");
                    }
//                        System.out.println();
                    } else {
                    System.out.println("Эта книга находится в библиотеке в настоящий момент");
                }
            } else {
                System.out.println("Посетитель не брал книг");
            }
        } else {
            System.out.println("Такой книги не было в библиотеке");
        }
    }

}