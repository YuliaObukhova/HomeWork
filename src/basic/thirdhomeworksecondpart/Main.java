package basic.thirdhomeworksecondpart;


public class Main {
    public static void main(String[] args) {

        Library PetrasLibrary = new Library();
        Book book1 = new Book("Будущее разума", "Митио Каку");
        Book book2 = new Book("Семь навыков высокоэффективных людей", "Стивен Кови");
        Book book3 = new Book("Сверхдержавы искусственного интеллекта. Китай, Кремниевая долина и новый мировой порядок", "Кай-Фу Ли");
        Book book4 = new Book("Машина, платформа, толпа. Наше цифровое будущее", "Эндрю Макафи, Эрик Бриньолфсон");
        Book book5 = new Book("Десять видений нашего будущего", "Кай-Фу Ли");

        //Test1
        System.out.println("//Test 1");
        PetrasLibrary.addBookInLibrary(book1);
        PetrasLibrary.addBookInLibrary(book2);
        PetrasLibrary.addBookInLibrary(book3);
        PetrasLibrary.addBookInLibrary(book4);
        PetrasLibrary.addBookInLibrary(book5);
        PetrasLibrary.addBookInLibrary(book4);
        System.out.println();
        PetrasLibrary.printLibrary();
        System.out.println();

        //Test2
        System.out.println("//Test 2");
        PetrasLibrary.deleteBook("Машина, платформа, толпа. Наше цифровое будущее");
        System.out.println();
        PetrasLibrary.printLibrary();
        System.out.println();
        PetrasLibrary.deleteBook("Семь навыков высокоэффективных людей");
        System.out.println();
        PetrasLibrary.printLibrary();
        System.out.println();
        PetrasLibrary.addBookInLibrary(book2);
        System.out.println();

        //Test3
        System.out.println("//Test 3");
        System.out.println(PetrasLibrary.findBook("Будущее разума").getName() + " " + PetrasLibrary.findBook("Будущее разума").getAuthor());
        System.out.println(PetrasLibrary.findBook("Десять видений нашего будущего").getName() + " " + PetrasLibrary.findBook("Десять видений нашего будущего").getAuthor());
        System.out.println();

        //Test4
        System.out.println("//Test 4");
        PetrasLibrary.findByAuthor("Кай-Фу Ли");
        System.out.println();
        PetrasLibrary.findByAuthor("Стивен Кови");
        System.out.println();

        //Test5
        System.out.println("//Test 5");
        Visitor vasya = new Visitor("Василий Васильев");
        Visitor ivan = new Visitor("Иван Иванов");

        PetrasLibrary.lendABook(new Book("Гении и аутсайдеры", "Малкольм Гладуэлл"), ivan);
        PetrasLibrary.lendABook(new Book("Гении и аутсайдеры", "Малкольм Гладуэлл"), vasya);
        PetrasLibrary.lendABook(book5, vasya);
        PetrasLibrary.lendABook(book1, ivan);
        PetrasLibrary.lendABook(book2, ivan);
        System.out.println();

        System.out.println("//Test 6");
        PetrasLibrary.returnBook(book3, ivan, 4); //у Ивана book1
        PetrasLibrary.returnBook(book2, vasya, 2);//у Василия book5
        System.out.println();
        PetrasLibrary.returnBook(book1, vasya, 5);//Другой посетитель пытается вернуть книгу
        System.out.println();
        System.out.println(book1.isTaken() ? "Книга не доступна" : "Книга доступна");
        System.out.println(vasya.isAlreadyGotTheBook() ? "У посетителя сейчас есть книга" : "У посетителя сейчас нет книги");
        System.out.println(ivan.isAlreadyGotTheBook() ? "У посетителя сейчас есть книга" : "У посетителя сейчас нет книги");

        System.out.println();
        PetrasLibrary.returnBook(book5, vasya, 5);
        PetrasLibrary.returnBook(book1, ivan, 2);
        System.out.println();

        System.out.println(vasya.isAlreadyGotTheBook() ? "У посетителя сейчас есть книга" : "У посетителя сейчас нет книги");
        System.out.println(ivan.isAlreadyGotTheBook() ? "У посетителя сейчас есть книга" : "У посетителя сейчас нет книги");
        System.out.println();
        System.out.println(book1.isTaken() ? "Книга не доступна" : "Книга доступна");
        System.out.println(book5.isTaken() ? "Книга не доступна" : "Книга доступна");
        System.out.println();

        //Test7
        System.out.println("//Test 7");
        PetrasLibrary.lendABook(book1, vasya);
        System.out.println();
        PetrasLibrary.lendABook(book5, ivan);
        System.out.println();

        PetrasLibrary.returnBook(book1, vasya, 5);
        PetrasLibrary.returnBook(book5, ivan, 4);
        System.out.println();

        System.out.println("Рейтинг книги " + book1.getName() + " " + book1.getAuthor() + " : " + book1.rating());
        System.out.println("Рейтинг книги " + book5.getName() + " " + book5.getAuthor() + " : " + book5.rating());
    }
}
