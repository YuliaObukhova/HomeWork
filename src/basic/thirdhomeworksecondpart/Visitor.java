package basic.thirdhomeworksecondpart;

public class Visitor {
    public String id = null;
    private final String name;
    private boolean alreadyGotTheBook;
    private Book takenBook;

    public Visitor(String name){
        this.name = name;
        this.alreadyGotTheBook = false;
        this.takenBook = null;
    }

    public Book getTakenBook() {
        return takenBook;
    }

    public void setTakenBook(Book takenBook) {
        this.takenBook = takenBook;
    }

    public boolean isAlreadyGotTheBook() {
        return alreadyGotTheBook;
    }

    public void setAlreadyGotTheBook(boolean alreadyGotTheBook) {
        this.alreadyGotTheBook = alreadyGotTheBook;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
