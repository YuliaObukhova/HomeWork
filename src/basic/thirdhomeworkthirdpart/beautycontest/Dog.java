package basic.thirdhomeworkthirdpart.beautycontest;


public class Dog {

    private final String name;

    Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
