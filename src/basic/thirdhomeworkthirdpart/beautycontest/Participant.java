package basic.thirdhomeworkthirdpart.beautycontest;


public class Participant {
    private final String dogOwnerName;
    private final String dogName;
    private final double middleScore;


    Participant(String dogOwnerName, String dogName, double middleScore) {
        this.dogOwnerName = dogOwnerName;
        this.dogName = dogName;
        this.middleScore = middleScore;
    }

    public String getDogOwnerName() {
        return dogOwnerName;
    }

    public String getDogName() {
        return dogName;
    }

    public double getMiddleScore() {
        return middleScore;
    }
}
