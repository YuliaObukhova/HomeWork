package basic.thirdhomeworkthirdpart.beautycontest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(); //количество участников

        System.out.println("Введите имена владельцев собак: ");
        ArrayList<String> dogsOwnersNames = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            dogsOwnersNames.add(sc.next());
        }
        System.out.println();

        System.out.println("Введите имена собак: ");
        ArrayList<Dog> dogs = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            dogs.add(new Dog(sc.next()));
        }
        System.out.println();

        System.out.println("Введите оценки собак: ");
        ArrayList<Double> middleScores = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            middleScores.add(((int) ((((double) sc.nextInt() + (double) sc.nextInt() + (double) sc.nextInt()) / 3) * 10)) / 10.0);
        }
        System.out.println();

        ArrayList<Participant> resultList = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            resultList.add(new Participant(dogsOwnersNames.get(i), dogs.get(i).getName(), middleScores.get(i)));
        }
        resultList.sort(Comparator.comparingDouble(Participant::getMiddleScore).reversed());
        for (int i = 0; i < 3; i++) { // 3 - количество призовых мест
            System.out.println(resultList.get(i).getDogOwnerName() + " : " + resultList.get(i).getDogName() + ", " + resultList.get(i).getMiddleScore());
        }

    }
}

