package basic.thirdhomeworkthirdpart.animals;

public class Dolphin extends Mammal implements Swimming {
    String name;

    Dolphin(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void swimming() {
        System.out.println(" плавает быстро");
    }
}
