package basic.thirdhomeworkthirdpart.animals;

public class Main {
    public static void main(String[] args) {
        Bat bat1 = new Bat("Летучая мышь");
        Dolphin dolphin1 = new Dolphin("Дельфин");
        GoldFish goldFish1 = new GoldFish("Золотая рыбка");
        Eagle eagle1 = new Eagle("Орёл");

        System.out.print(bat1.getName());
        bat1.eat();

        System.out.print(dolphin1.getName());
        dolphin1.eat();

        System.out.print(goldFish1.getName());
        goldFish1.eat();

        System.out.print(eagle1.getName());
        eagle1.eat();
        System.out.println();

        System.out.print(bat1.getName());
        bat1.sleep();

        System.out.print(dolphin1.getName());
        dolphin1.sleep();

        System.out.print(goldFish1.getName());
        goldFish1.sleep();

        System.out.print(eagle1.getName());
        eagle1.sleep();
        System.out.println();

        System.out.print(bat1.getName());
        bat1.wayOfBirth();

        System.out.print(dolphin1.getName());
        dolphin1.wayOfBirth();

        System.out.print(goldFish1.getName());
        goldFish1.wayOfBirth();

        System.out.print(eagle1.getName());
        eagle1.wayOfBirth();
        System.out.println();

        System.out.print(bat1.getName());
        bat1.flying();

        System.out.print(dolphin1.getName());
        dolphin1.swimming();

        System.out.print(goldFish1.getName());
        goldFish1.swimming();

        System.out.print(eagle1.getName());
        eagle1.flying();
    }
}
