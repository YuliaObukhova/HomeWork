package basic.thirdhomeworkthirdpart.animals;

public class Bat extends Mammal implements Flying {
    private String name;

    Bat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void flying() {
        System.out.println(" летает медленно");
    }
}
