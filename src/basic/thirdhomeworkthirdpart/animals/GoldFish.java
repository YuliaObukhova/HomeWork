package basic.thirdhomeworkthirdpart.animals;

public class GoldFish extends Fish implements Swimming {
    String name;

    GoldFish(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void swimming() {
        System.out.println(" плавает медленно");
    }
}
