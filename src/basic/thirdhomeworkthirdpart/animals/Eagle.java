package basic.thirdhomeworkthirdpart.animals;

public class Eagle extends Bird implements Flying {
    String name;

    Eagle(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public void flying() {
        System.out.println(" летает быстро");
    }
}
