package basic.thirdhomeworkthirdpart.bestcarpenterever;

public class BestCarpenterEver {
    /**
     * проверяет можно ли починить эту мебель
     *
     * @param visitorsFurniture мебель, которую необходимо починить
     * @return можно или нельзя починить
     */
    public boolean isItPossibleToRepair(Furniture visitorsFurniture) {
        return visitorsFurniture instanceof Stool;
    }
}