package basic.thirdhomeworkthirdpart.bestcarpenterever;

public class Main {
    public static void main(String[] args) {
        Furniture stool1 = new Stool();
        Furniture table1 = new Table();

        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();
        System.out.println(bestCarpenterEver.isItPossibleToRepair(stool1));
        System.out.println(bestCarpenterEver.isItPossibleToRepair(table1));
    }
}

