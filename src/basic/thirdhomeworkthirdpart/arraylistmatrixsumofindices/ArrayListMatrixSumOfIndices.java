package basic.thirdhomeworkthirdpart.arraylistmatrixsumofindices;

import java.util.ArrayList;
import java.util.Scanner;

//На вход передается N — количество столбцов в двумерном массиве
// и M — количество строк. Необходимо вывести матрицу на экран,
// каждый элемент которого состоит из суммы индекса столбца и строки
// этого же элемента. Решить необходимо используя ArrayList.
public class ArrayListMatrixSumOfIndices {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(); // количество столбцов
        int m = sc.nextInt(); //  количество строк

        ArrayList<Integer> row = new ArrayList<>();
        int temp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int sum = i + j;
                row.add(sum);
                System.out.print(row.get(temp) + " ");
                temp++;
            }
            System.out.println();
        }
    }
}
